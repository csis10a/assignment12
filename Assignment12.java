import java.util.Scanner;

public class Assignment12 {

    public static void main(String[] args) {   
        System.out.print("\f");
        // Do NOT edit anything in the main method. Your edits will be 
        // made in the methods that have the prefix "problem"

        problem1();       
        problem2();        
        problem3();    
        problem4();   
        problem5();
        problem6();
        problem7();
        problem8();       
        problem9();     
        problem10(); // Extra Credit
        problem11(); // Extra Credit            
    }

    // 2) write randomInt method here:


    // 7) write your bar method here:
    //     pseudocode:
    //       void method bar, takes parameter int length
    //         has for loop that repeats length times
    //             prints 1 "X" to console
    //         after loop, goes to new line  (println)


    // 9) write your makeHist method here:
    //     pseudocode:
    //       void method makeHist, takes parameter array of int scores
    //         create a counts array as a histogram of scores (like prob6)
    //         make a loop that prints a bar chart of the counts array 
    //              (like prob9)
    //              (use your bar method to draw the horizontal lines)
    

//     public static int[] randomArray(int n) 
//     { 
//         int[] a = new int[n]; 
//         for (int i = 0; i<a.length; i++) 
//         { 
//             a[i] = randomInt(0, 100); 
//         } 
//         return a; 
//     }

    public static void problem1() {
        // 1) 
        System.out.println("\n-- Problem 1 ----------");
        for (int i = 0; i < 10; i++) {
            double x = Math.random(); 
            System.out.println(x); 
        }
        
        // a) Copy above loop here, modify so x is between  [0,10)  
     
        
        // b) Copy above loop here, modify so x is between  [15,25)

        
        // c) Copy above loop here, modify so x is between  [70, 100)
         
        
        // what is the trick to solving these? 
        //    first determine the spread of the numbers (high - low)
        //    then determine how far to shift them from 0 (low)
        //    so in general, we multiply Math.random() by the spread 
        //     and add the shift. 
        //    Use these insights to solve problem 2
    }
  


    public static void problem2() {
        System.out.println("\n-- Problem 2 ----------");
        // 2)  Write the definition of randomInt below main
        //     do not change this loop!  should produce nums from 25 to 39 

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        for (int i = 0; i < 10; i++) {
            System.out.println(randomInt(25,40)); 
        }
        */
    }

    public static void problem3() {
        System.out.println("\n-- Problem 3 ----------");
        // 3)  Uncomment randomArray method below main

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        int numValues = 8; 
        int[] quizScores = randomArray(numValues); 
        printArray(quizScores);
        */
        
        // Run the above, THEN modify to produce an array of
        // size 20. THEN modify randomArray so the 
        // numbers in the array are between [0,10)
    }

    public static void problem4() {
        System.out.println("\n-- Problem 4 ----------"); 
        // 4) write your own loop to count how many values in 
        //    quizScores are between [3,8)

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*        
        int[] quizScores = randomArray(8);   
        
        System.out.println("There are " + num + " values between 3 and 8");
        */
    }

    public static void problem5() {
        System.out.println("\n-- Problem 5 ----------");
        // 5) Use the inRange method to do the same thing as 4)

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        System.out.println("There are " + num + " values between 3 and 8");
        */
    }

    public static void problem6() {
        System.out.println("\n-- Problem 6 ----------");
        // 6) Create array counts as a histogram of quizScores
         
        int [] counts = new int[10];   // only go up to 10 
                                       // because that's the
                                       // highest quizScore you'll find

        // write a loop to tabulate the counts histogram here:
                                       
        printArray(counts);        
    }

    public static void problem7() {
        System.out.println("\n-- Problem 7 ----------");
        // 7) write a void method bar below main that uses a 
        //    loop to print a horizontal line of 'X's  
        //      (print 1 "X" each time through the loop)

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        bar(5);  // should print  XXXXX
        bar(8);  // should print  XXXXXXXX
        */
    }

    public static void problem8() {
        System.out.println("\n-- Problem 8 ----------");
        // write a loop that prints a bar chart of the counts array
    }

    public static void problem9() {
        System.out.println("\n-- Problem 9 ----------");
        // encapsulate your answers to 6 and 8 into a method makeHist
        //  demo your method here:

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        int [] arr1 = randomArray(50);
        System.out.println("Array 1 Histogram: ");
        makeHist(arr1);
        int [] arr2 = randomArray(25);
        System.out.println("Array 2 Histogram: ");
        makeHist(arr2);
        */
    }

    public static void problem10() {
        System.out.println("\n-- Problem 10 --------- (Extra Credit)");
        // write a method that finds a target in an array, 
        //   returning the index of the target or -1 if not found
        int [] arr3 = {0,10,20,99,40,50,60,70};

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        System.out.println("Position of 99 is " + find(arr3, 99)); 
         // should say 3
        System.out.println("And here is the 99 value: " + arr3[find(arr3, 99)]);
         // should say 99
         
        System.out.println("Position of 55 is " + find(arr3, 55)); 
         // should say -1  
         */
    }

    public static void problem11() {
        System.out.println("\n-- Problem 11 --------- (Extra Credit)");
        // write a method that sorts an array using BubbleSort or 
        //  SelectionSort

        // REMOVE BLOCK COMMENT /* and */ TO ENABLE CODE BELOW
        /*
        sort(quizScores);
        printArray(quizScores);
        */
    }

    public static void printArray(int[] a)  { 
        for (int i = 0; i < a.length; i++) { 
            System.out.println(a[i]); 
        } 
    }
    
    public static int inRange(int[] a, int low, int high) { 
        int count = 0; 
        for (int i = 0; i < a.length; i++) { 
            if (a[i] >= low && a[i] < high) {
                count++; 
            }
        } return count; 
    }    
}
