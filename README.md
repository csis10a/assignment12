# CSIS 10A - Assignment 12

__Prerequisite: Lecture or Reading on Chapter 12__


Clone the project or download and extract the Assignment12.zip to your workspace. Then open the project.blueJ file to and execute the Assignment12 class. This class has a number of array related exercises inside it. Begin these exercises, listed as 1 through 9. When you are finished you may wish to complete extra credit exercises 10 and 11.

When you are finished, make a jar and upload your project to the server __or__ create a git repository, commit your changes to it and email your teacher to the link for your repository.